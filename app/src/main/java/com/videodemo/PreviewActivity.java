package com.videodemo;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;

/**
 * Created by Bharat on 08/15/2016.
 */
public class PreviewActivity extends FragmentActivity implements View.OnClickListener {

    private ImageView img_captured, img_dustbin, img_text, img_icon, img_download, img_attach, img_send;

    private int type = 0;
    private String filePath = "";
    private File file;

    private VideoView videoView;

    private boolean fileDelete = true;
    private boolean fileSaved = false;

    public static final int FILE_TYPE_IMAGE = 0, FILE_TYPE_VIDEO = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_preview_activity);
        type = getIntent().getExtras().getInt("from");
        filePath = getIntent().getExtras().getString("file");
        file = new File(filePath);
        initView();
        setValues();
    }

    private void setValues() {
        if (type == FILE_TYPE_IMAGE) {
            img_captured.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.GONE);
            img_captured.setImageBitmap(VideoRecordingPreLollipopActivity.decodeFile(file));
        } else {
            img_captured.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);
            videoView.setMediaController(new MediaController(this));
            videoView.setVideoURI(Uri.fromFile(file));
            videoView.requestFocus();
            videoView.start();
        }
    }

    private void initView() {
        img_captured = (ImageView) findViewById(R.id.img_captured);
        videoView = (VideoView) findViewById(R.id.videoView);

        img_dustbin = (ImageView) findViewById(R.id.img_dustbin);
        img_dustbin.setOnClickListener(this);
        img_text = (ImageView) findViewById(R.id.img_text);
        img_text.setOnClickListener(this);
        img_icon = (ImageView) findViewById(R.id.img_icon);
        img_icon.setOnClickListener(this);
        img_download = (ImageView) findViewById(R.id.img_download);
        img_download.setOnClickListener(this);
        img_attach = (ImageView) findViewById(R.id.img_attach);
        img_attach.setOnClickListener(this);
        img_send = (ImageView) findViewById(R.id.img_send);
        img_send.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_dustbin:
                try {
                    if (file != null)
                        file.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
                break;

            case R.id.img_text:
                showTextAlert();
                break;

            case R.id.img_icon:
                break;

            case R.id.img_download:
                if (!fileSaved) {
                    fileSaved = true;
                    fileDelete = false;
                    Toast.makeText(PreviewActivity.this, type == FILE_TYPE_IMAGE ? "Image" : "Video" + " saved at location : " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(PreviewActivity.this, type == FILE_TYPE_IMAGE ? "Image" : "Video" + " already saved.", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.img_attach:
                break;

            case R.id.img_send:
                break;
        }
    }

    private String shareText = "";

    private void showTextAlert() {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_text_share, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView.findViewById(R.id.et_text);
        userInput.setText(shareText);
        userInput.setSelection(shareText.length());

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                // edit text
                                shareText = userInput.getText().toString();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        try {
            if (fileDelete) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }
}
