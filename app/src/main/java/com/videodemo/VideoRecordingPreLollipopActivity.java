package com.videodemo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.DonutProgress;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Bharat on 08/16/2016.
 */
/* code for video recording custom */
public class VideoRecordingPreLollipopActivity extends FragmentActivity implements SurfaceHolder.Callback, View.OnClickListener, MediaRecorder.OnInfoListener {

    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    public MediaRecorder mediaRecorder = new MediaRecorder();
    public static Camera mCamera;

    private Bitmap bitmap = null;

    ImageView img_close, img_capture, img_switch_camera, img_flash;
    DonutProgress donut_progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_layout);
        init();
    }

    private void init() {
        surfaceView = (SurfaceView) findViewById(R.id.surface_camera);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        img_close = (ImageView) findViewById(R.id.img_close);
        img_close.setOnClickListener(this);
        img_capture = (ImageView) findViewById(R.id.img_capture);
        img_capture.setOnClickListener(this);
        img_capture.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d("test", "capture onLongClick");
                startRecordingVideo();
                return false;
            }
        });
        img_switch_camera = (ImageView) findViewById(R.id.img_switch_camera);
        img_switch_camera.setOnClickListener(this);
        img_flash = (ImageView) findViewById(R.id.img_flash);
        img_flash.setSelected(false);
        img_flash.setOnClickListener(this);

        donut_progress = (DonutProgress) findViewById(R.id.donut_progress);
    }

    ByteArrayInputStream fis2;
    FileOutputStream fos;
    Bitmap bmp, bmp1;
    ByteArrayOutputStream bos;
    BitmapFactory.Options options;

    private boolean safeToTakePicture = false;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_capture:
                Log.d("test", "capture clicked");

                if (!isRecording) {
                    mCamera.takePicture(null, null
                            , new Camera.PictureCallback() {
                                @Override
                                public void onPictureTaken(byte[] data, Camera camera) {
                                    Log.d("test", "PictureCallback onPictureTaken called");
                                    File tmpFile = new File(getImageFilePath());
                                    try {
                                        fos = new FileOutputStream(tmpFile);
                                        fos.write(data);
                                        fos.close();
                                    } catch (FileNotFoundException e) {
                                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                    } catch (IOException e) {
                                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                    }
                                    options = new BitmapFactory.Options();
                                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                                    bmp1 = decodeFile(tmpFile);
                                    bmp = Bitmap.createScaledBitmap(bmp1, surfaceView.getWidth(), surfaceView.getHeight(), true);
                                    callingStartActivity(tmpFile, 0);
                                }
                            });
                } else {
                    stopRecordingVideo();
                    Log.d("test", "stopRecordingVideo called");
                }
                break;

            case R.id.img_close:
                finish();
                break;

            case R.id.img_flash:
                img_flash.setSelected(!img_flash.isSelected());
                startBackgroundThread();
                setFlashMode();
                break;

            case R.id.img_switch_camera:
//                cameraId = cameraId == 0 ? 1 : 0;
                if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                } else {
                    cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                }
                Log.d("test", "cameraId => " + cameraId);
                startBackgroundThread();
                releaseMediaRecorder();
                releaseCamera();
                openCamera();
                initCamera();
                break;
        }

    }


    Camera.ShutterCallback myShutterCallback = new Camera.ShutterCallback() {

        public void onShutter() {
            // TODO Auto-generated method stub
        }
    };

    Camera.PictureCallback myPictureCallback_RAW = new Camera.PictureCallback() {

        public void onPictureTaken(byte[] arg0, Camera arg1) {
            // TODO Auto-generated method stub
        }
    };

    Camera.PictureCallback myPictureCallback_JPG = new Camera.PictureCallback() {

        public void onPictureTaken(byte[] arg0, Camera arg1) {
            // TODO Auto-generated method stub
        }
    };

    boolean onceCalled = false;

    private void callingStartActivity(File tmpFile, int type) {
        if (!onceCalled) {
            onceCalled = true;
            Log.d("test", "file path => " + tmpFile.getAbsolutePath() + ", type = " + type);
            Intent intent = new Intent(this, PreviewActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("from", type);
            bundle.putString("file", tmpFile.getAbsolutePath());
            intent.putExtras(bundle);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
            finish();
        }
    }

    private String getImageFilePath() {
        File file = new File(Environment.getExternalStorageDirectory()
                .getPath(), "fitplus/SendFiles");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = System.currentTimeMillis() + ".png";
        File file1 = null;
        try {
            file1 = new File(file.getAbsolutePath() + "/" + uriSting);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            file1.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("test", "getImageFilePath => " + file1.getAbsolutePath());
        return file1.getAbsolutePath();
    }

    public String getVideoFilename() {
        File file1 = new File(Environment.getExternalStorageDirectory()
                .getPath(), "fitplus/SendFiles");
        if (!file1.exists()) {
            file1.mkdirs();
        }
        String uriSting = System.currentTimeMillis() + ".mp4";
        File file = null;
        try {
            file = new File(file1.getAbsolutePath() + "/" + uriSting);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("test", "getVideoFilename => " + file.getAbsolutePath());
        return file.getAbsolutePath();
    }

    public static Bitmap decodeFile(File f) {
        Bitmap b = null;
        BitmapFactory.Options options, o, o2;
        FileInputStream fis;
        try {
            // Decode image size
            o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
            int IMAGE_MAX_SIZE = 1000;
            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(
                        2,
                        (int) Math.round(Math.log(IMAGE_MAX_SIZE
                                / (double) Math.max(o.outHeight, o.outWidth))
                                / Math.log(0.5)));
            }

            // Decode with inSampleSize
            o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return b;
    }

    private String videoFileName = "";
    Camera.Size previewSize = null;
    int width, height, cameraId = 0;

    @SuppressLint("NewApi")
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d("test", "surfaceChanged called");
        this.surfaceHolder = holder;
        this.width = width;
        this.height = height;
        initCamera();
    }


    private void openCamera() {
        try {
            mCamera = Camera.open(cameraId);
        } catch (Exception e) {
            e.printStackTrace();
            cameraId = findFrontFacingCamera();
            mCamera = Camera.open(cameraId);
            mCamera.release();
            mCamera = null;
        }
        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(surfaceHolder);
            } catch (IOException e) {
                e.printStackTrace();
                mCamera.release();
                mCamera = null;
            }
        } else {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseMediaRecorder();
        releaseCamera();
    }

    private void initCamera() {
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            List<Camera.Size> supportSize = parameters
                    .getSupportedPreviewSizes();
            if (supportSize != null) {
                previewSize = getOptimalPreviewSize(supportSize, width,
                        height);
            }
            parameters.setPreviewSize(previewSize.width, previewSize.height);
            parameters.set("orientation", "portrait");
            mCamera.setDisplayOrientation(90);
            mCamera.setPreviewDisplay(surfaceHolder);
            mCamera.setParameters(parameters);
            setFlashMode();
            mCamera.startPreview();
            mCamera.lock();
            mCamera.unlock();
            safeToTakePicture = true;

            mediaRecorder = new MediaRecorder();
            mediaRecorder.setCamera(mCamera);
            mediaRecorder.setPreviewDisplay(surfaceHolder.getSurface());
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);
            mediaRecorder.setMaxDuration(10000);
            mediaRecorder.setOnInfoListener(this);
            mediaRecorder.setVideoSize(previewSize.width, previewSize.height);
            mediaRecorder.setVideoFrameRate(30);
            mediaRecorder.setOrientationHint(90);
            mediaRecorder.setVideoEncodingBitRate(500);
            mediaRecorder.setAudioEncodingBitRate(128);

            videoFileName = getVideoFilename();
            mediaRecorder.setOutputFile(videoFileName);
//            mediaRecorder.prepare();
//            mediaRecorder.start();
//            mediaRecorder.start();
            try {
                mediaRecorder.prepare();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                Log.d("ERROR ", "IllegalStateException");
                e.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.d("ERROR ", "IOException");
                e.printStackTrace();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private boolean isRecording = false;

    private void startRecordingVideo() {
        try {
            isRecording = true;
//            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopRecordingVideo() {
        try {
            isRecording = false;
            mediaRecorder.release();
            mediaRecorder.stop();
            mediaRecorder.reset();
            callingStartActivity(new File(videoFileName), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;


    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setFlashMode() {
        Log.d("test", "setFlashMode called");
        try {
            if (mCamera != null) {
                mCamera.cancelAutoFocus();
                Camera.Parameters p = mCamera.getParameters();
                if (img_flash.isSelected()) {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                } else {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                }
                mCamera.setParameters(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d("test", "surfaceCreated called");
//        cameraId = findFrontFacingCamera();
        this.surfaceHolder = holder;
        startBackgroundThread();
        openCamera();
    }


    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null)
            return null;
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d("test", "surfaceDestroyed called");
        // releaseMediaRecorder();
        if (mediaRecorder != null) {
            releaseMediaRecorder();
            releaseCamera();
        }
    }

    private void releaseMediaRecorder() {
        try {
            if (mediaRecorder != null) {
                mediaRecorder.stop(); // stop recording
                mediaRecorder.reset(); // set state to idle
                mediaRecorder.release(); // release resources back to the
                // system
                mediaRecorder = null;// release the recorder object
            }


        } catch (Exception e) {
            Log.i("error in", "releasing mediarecorder");
        }
    }

    private void releaseCamera() {
        try {
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mCamera.release(); // release the camera for other
                // applications
//                mCamera = null;
            }
        } catch (Exception e) {
            Log.i("error in", "releasing camera");
        }

    }

    /* end code for video recording */
    public void rotateImage(int angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private void sending_recordedvideo(String str_ImagePath2) {
        File file = new File(str_ImagePath2);
        long length = file.length() / (1024 * 1024);
        System.out.println("file size" + length + " mb");
    }

    private int findFrontFacingCamera() {
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        Log.d("test", "findFrontFaceingCamera => " + cameraId);
        return cameraId;
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        Log.d("test", "onInfo => what = " + what + ", extra = " + extra);
        callingStartActivity(new File(videoFileName), 1);
    }
}
